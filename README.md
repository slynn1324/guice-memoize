#guice-memoize

Provides a simple Guice-AOP extension to provide method [Memoization](http://en.wikipedia.org/wiki/Memoization).  Caching is implemented with Guava's Cache implementation.

##Setup

Intall the _MemoizeModule_ into your application's Guice module.

	protected void configure() {

		install(new MemoizeModule());
		
		// other bindings...
	}

##Use

Simply annotate your methods with the _@Memoize_ annotation.


	public class Greeter{

		@Memoize
		public void sayHello(String name){
			return "Hello " + name;
		}
	}


##More

See _Memoize.java_ and the Guava [CachesExplained](https://code.google.com/p/guava-libraries/wiki/CachesExplained) article for more details on the available configuration options on the _@Memoize_ annotation.


##License

guice-memoize is released under the [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.html).

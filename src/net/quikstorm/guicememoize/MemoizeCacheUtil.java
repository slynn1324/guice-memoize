package net.quikstorm.guicememoize;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Singleton wrapper around the cache map, which provides functionality to retrieve and
 * create caches, as well as retrieve information about them, and invalidate entries in the caches.
 * 
 * <p>
 * The {@MemoizeCacheUtil} is mapped as a {@Singleton} by the {@MemoizeModule}, so to obtain a
 * reference to this class, you should inject a refernece to this instance from the same Guice 
 * injector that is configured with the {@MemoizeModule} and performing the AOP intercepting.
 * 
 * @author slynn1324
 */
@Singleton
public class MemoizeCacheUtil {

	private static final Logger log = LoggerFactory.getLogger(MemoizeCacheUtil.class);
	
	private final Map<Method, Cache<String,Optional<?>>> cacheMap;
	
	public MemoizeCacheUtil(){
		cacheMap = Maps.newConcurrentMap();
	}
	
	/**
	 * Returns the cache from the provided MethodInvocation. 
	 * 
	 * This method is the primary method used by the {@MemoizeMethodInterceptor}
	 * and should be the most efficient. 
	 * 
	 * If the cache does not already exist, then it will be created and added to
	 * the CacheMap.
	 * 
	 * @param invocation
	 * @return either the existing {@Cache} or a new {@Cache} 
	 */
	public Cache<String,Optional<?>> getCache(MethodInvocation invocation){
		Cache<String,Optional<?>> c = cacheMap.get(invocation.getMethod());		
		if ( c == null ){
			
			Memoize m = invocation.getMethod().getAnnotation(Memoize.class);
			
			if ( m != null ){ // sanity check here?
				if ( log.isInfoEnabled() ){
					log.info("Creating memoization cache for method: " + invocation.getMethod());
					
					String msg = Joiner.on("").join(
						" Memoization Configuration [",
						"maximumSize=",
						(m.maximumSize() >= 0 ? m.maximumSize() : "default"),
						(" expireAfterWriteSeconds="),
						(m.expireAfterWriteSeconds() >= 0 ? m.expireAfterWriteSeconds() : "default"),
						(" expireAfterAccessSeconds="),
						(m.expireAfterAccessSeconds() >= 0 ? m.expireAfterAccessSeconds() : "default"),
						(" initialCapacity="),
						(m.initialCapacity() >= 0 ? m.initialCapacity() : "default"),
						(" concurrencyLevel="),
						(m.concurrencyLevel() >= 0 ? m.concurrencyLevel() : "default"),
						"]"
						);
						
					log.info(msg);
				}
				CacheBuilder<Object,Object> builder = CacheBuilder.newBuilder();
				
				if ( m.maximumSize() >= 0 ){
					builder.maximumSize(m.maximumSize());
				}
				if ( m.expireAfterWriteSeconds() >= 0 ){
					builder.expireAfterWrite(m.expireAfterWriteSeconds(), TimeUnit.SECONDS);
				}
				if ( m.expireAfterAccessSeconds() >= 0 ){
					builder.expireAfterAccess(m.expireAfterAccessSeconds(), TimeUnit.SECONDS);
				}
				if ( m.initialCapacity() >= 0 ){
					builder.initialCapacity(m.initialCapacity());
				}
				if ( m.concurrencyLevel() >= 0 ){
					builder.concurrencyLevel(m.concurrencyLevel());
				}
				
				c = builder.build();
			
				cacheMap.put(invocation.getMethod(),c);
			}
		}
		return c;
	}
	
	/**
	 * Returns the names for each known cache.
	 * @return a list of each cache name
	 */
	public List<String> listCacheNames(){
		List<String> names = Lists.newArrayList();
		
		for (Method m : cacheMap.keySet()){
			names.add(m.getDeclaringClass().getName() + "." + m.getName());
		}
		
		return names;
	}
	
	/**
	 * Returns a {@MemoizeCacheInfo} for a named cache. If the cache name does not exist,
	 * then will return null.
	 * 
	 * @param cacheName
	 * @return the {@MemoizeCacheInfo} for the named cache
	 */
	public MemoizeCacheInfo getCacheInfo(String cacheName){
		Cache<String, Optional<?>> cache = getCacheByName(cacheName);
		
		if ( cache == null ){
			return null;
		}
		
		MemoizeCacheInfo cacheInfo = new MemoizeCacheInfo();
		cacheInfo.setCacheName(cacheName);
		cacheInfo.setStats(cache.stats());
		cacheInfo.setSize(cache.size());
		
		return cacheInfo;
		
	}
	
	/**
	 * Lists the caches by {@MemoizeCacheInfo} objects.
	 * @return a list containing a {@MemoizeCacheInfo} for each known cache.
	 */
	public List<MemoizeCacheInfo> listCacheInfo(){
		List<MemoizeCacheInfo> list = Lists.newArrayList();
		
		List<String> cacheNames = listCacheNames();
		
		for ( String name : cacheNames ){
			list.add(getCacheInfo(name));
		}
		
		return list;
	}
	
	/**
	 * Invalidates all of the entries in a cache by name.
	 * 
	 * @param name - the name of the cache
	 */
	public void invalidateAllEntriesInCache(String name){
		Cache<String,Optional<?>> theCache = getCacheByName(name);
		
		if ( theCache != null ){
			theCache.invalidateAll();
		}
		
	}
	
	/**
	 * Invalidates a particular entry in the cache. The key into the cache
	 * can be created by passing the parameters, in order, to the {@link MemoizeCacheUtil#createKey(Object...)} method.
	 * @param name - the cache name
	 * @param keys - the keys to invalidate
	 */
	public void invalidateEntriesInCache(String name, String... keys){
		Cache<String,Optional<?>> theCache = getCacheByName(name);
		
		if ( theCache != null ){
			theCache.invalidateAll(Lists.newArrayList(keys));
		}
	}
	
	/**
	 * Retrieve a cache instance by name
	 * @param name
	 * @return the cache instance
	 */
	public Cache<String,Optional<?>> getCacheByName(String name){
		
		for (Method m : cacheMap.keySet()){
			String cacheName = m.getDeclaringClass().getName() + "." + m.getName();
			if ( name.equals(cacheName) ){
				// return here to short-circuit the for loop
				return cacheMap.get(m);
			}
		}
		
		return null;
	}
	
	/**
	 * Creates the cache key from a list of objects. For a key to be unique, each argument
	 * must have a toString that creates a unique value that fully represents the object.
	 * @param args
	 * @return a string to be used as the cache key.
	 */
	public String createKey(Object... args){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < args.length; ++i ){
			sb.append(i).append(":").append(args[i]).append(",");
		}
		return sb.toString();
	}
	
}

package net.quikstorm.guicememoize;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

/**
 * Establishes the default binding to intercept all methods in any class that are annotated with the
 * {@link Memoize} annotation.
 * 
 * @author slynn1324
 *
 */
public class MemoizeModule extends AbstractModule {

	@Override
	protected void configure() {
		// we use the binder's getProvider() methdo to get a provider of the 
		// MemoizeCacheUtil, which is bound as a Singleton, so that we can
		// pass the dependency on, since Interceptors can't be injected
		// directly
		bindInterceptor(Matchers.any(), 
				Matchers.annotatedWith(Memoize.class), 
				new MemoizeInterceptor(getProvider(MemoizeCacheUtil.class)));
	}

}

/**
 * 
 */
package net.quikstorm.guicememoize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Identify that a method should be Memoized (cached) based on the parameter values.
 * 
 * <p>
 * For the memoization cache to work correctly, all parameters of the method to be memoized must 
 * have a valid toString() method that for objects that should be considered equal, return strings 
 * that are considered equal. Failure to meet this may cause cache misses where one would have 
 * expected hits and more dangerously, hits where one may have expected misses.
 * 
 * @author slynn1324
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Memoize {	
	int maximumSize() default -1;
	long expireAfterWriteSeconds() default -1;
	long expireAfterAccessSeconds() default -1;
	int concurrencyLevel() default -1;
	int initialCapacity() default -1;
	
}
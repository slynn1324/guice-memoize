package net.quikstorm.guicememoize;

import com.google.common.cache.CacheStats;

public class MemoizeCacheInfo {

	private String cacheName;
	private long size;
	private CacheStats stats;
	
	public String getCacheName() {
		return cacheName;
	}
	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public CacheStats getStats() {
		return stats;
	}
	public void setStats(CacheStats stats) {
		this.stats = stats;
	}
	
	
}

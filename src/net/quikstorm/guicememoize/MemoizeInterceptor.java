package net.quikstorm.guicememoize;

import javax.inject.Provider;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;
import com.google.common.cache.Cache;

/**
 * The actual Memoization aspect to intercept method calls.  
 *
 * <p>
 * Caching is based on a Guava Manual Cache where a separate cache is created
 * for each unique method that is intercepted. Details for the construction and
 * configuration of the Cache is extracted from the {@link Memoize} annotation
 * that also indicates that the method should be intercepted.
 * 
 * <p>
 * To access the cache contents, or to invalidate entries, see {@MemoizeCacheUtil}. 
 * 
 * <p>
 * WARNING: For a method to be eligible for memoization, the parameters must all have
 * toString() implementations that return values that adequately represent their complete value,
 * as the strings are used to create the cache keys.
 * 
 * @author slynn1324
 *
 */
public class MemoizeInterceptor implements MethodInterceptor{

	private static final Logger log = LoggerFactory.getLogger(MemoizeInterceptor.class);
	
	// keep a map of all of the caches... 
	private final Provider<MemoizeCacheUtil> cacheUtilProvider;
	
	// MethodInterceptors can't be Guice injected normally, but the behavior
	// is emulated in the MemoizeModule, and a reference to the singleton
	// MemoizeCacheUtil is provided in the constructor.
	//@Inject
	public MemoizeInterceptor(Provider<MemoizeCacheUtil> cacheUtilProvider){
		this.cacheUtilProvider = cacheUtilProvider;
	}
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {

		MemoizeCacheUtil cacheUtil = cacheUtilProvider.get();
		
		Cache<String,Optional<?>> cache = cacheUtil.getCache(invocation);
		
		// sanity check - getCache will return null if we can't find the @Memoize annotation
		if ( cache == null ){
			log.warn("Can't find the @Memoize annotation, just passing through...");
			return invocation.proceed();
		} 
		
		String key = cacheUtil.createKey(invocation.getArguments());

		// the cache will return null if not present, or an Optional<?> if missing
		Optional<?> cachedResult = cache.getIfPresent(key);
		
		Object result = null;
		
		if ( cachedResult == null ){
			if ( log.isDebugEnabled() ){
				log.debug("cache miss for key " + key);
			}
			result = invocation.proceed();
			// store an Optional<?> (to differentiate from null in the cache).
			cache.put(key, Optional.fromNullable(result));
		} else {
			if ( log.isDebugEnabled() ){
				log.debug("cache hit for key " + key);
			}
			// get back the value from the Optional<?> with possibly holding null
			result = cachedResult.orNull();
		}
		
		return result;
		
	}
	
	
	
}

package net.quikstorm.guicememoize;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Test cases for the MemoizeModule
 * 
 * @author slynn1324
 */
public class MemoizeTest {

	public static class ShouldMemoizeHelper{
		@Memoize
		public String sayHello(String name){
			return "Hello " + name;
		}
	}
	
	@Test
	public void shouldInvalidateEntry(){
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldMemoizeHelper help = injector.getInstance(ShouldMemoizeHelper.class);
		String msg = help.sayHello("John");
		String msg2 = help.sayHello("John");
		assertTrue("Should be the same instance due to memoization.", msg == msg2);
		
		MemoizeCacheUtil cacheUtil = injector.getInstance(MemoizeCacheUtil.class);
		
		List<String> caches = cacheUtil.listCacheNames();
		assertEquals("Should be 1 cache", 1, caches.size());
		
		assertEquals(caches.get(0), "net.quikstorm.guicememoize.MemoizeTest$ShouldMemoizeHelper.sayHello");
		
		cacheUtil.invalidateEntriesInCache(caches.get(0), cacheUtil.createKey("John"));
		
		String msg3 = help.sayHello("John");
		assertTrue("Should NOT be the samle instance because we invalidated the entry.", msg != msg3);
	}
	
	@Test
	public void shouldInvalidateAllEntries(){
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldMemoizeHelper help = injector.getInstance(ShouldMemoizeHelper.class);
		String msg = help.sayHello("John");
		String msg2 = help.sayHello("Susan");
		
		MemoizeCacheUtil cacheUtil = injector.getInstance(MemoizeCacheUtil.class);
		
		List<String> caches = cacheUtil.listCacheNames();
		
		cacheUtil.invalidateAllEntriesInCache(caches.get(0));
		
		MemoizeCacheInfo cacheInfo = cacheUtil.getCacheInfo(caches.get(0));
		assertEquals(0, cacheInfo.getSize());
		
		String msg3 = help.sayHello("John");
		assertTrue("Should NOT be the same instance because we invalidated the entry.", msg != msg3);
		assertEquals("The values should be equal.", msg, msg3);
		
		String msg4 = help.sayHello("Susan");
		assertTrue("Should NOT be the same instance because we invalidated the entry.", msg2 != msg4);
		assertEquals("The values should be equal.", msg2, msg4);
	}
	
	
	
	@Test
	public void shouldListNames(){
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldMemoizeHelper help = injector.getInstance(ShouldMemoizeHelper.class);
		String msg = help.sayHello("John");
		String msg2 = help.sayHello("John");
		assertTrue("Should be the same instance due to memoization.", msg == msg2);
		
		MemoizeCacheUtil cacheUtil = injector.getInstance(MemoizeCacheUtil.class);
		
		List<String> caches = cacheUtil.listCacheNames();
		assertEquals("Should be 1 cache", 1, caches.size());
		
		assertEquals(caches.get(0), "net.quikstorm.guicememoize.MemoizeTest$ShouldMemoizeHelper.sayHello");
	}
	
	@Test
	public void shouldMemoize(){
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldMemoizeHelper help = injector.getInstance(ShouldMemoizeHelper.class);
		
		String msg = help.sayHello("John");
		String msg2 = help.sayHello("John");
		assertTrue("Should be the same instance due to memoization.", msg == msg2);
	}
	
	public static class ShouldNotMemoizeHelper{
		public String sayHello(String name){
			return "Hello " + name;
		}
	}
	
	@Test
	public void shouldNotMemoize(){
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldNotMemoizeHelper help = injector.getInstance(ShouldNotMemoizeHelper.class);
		
		String msg = help.sayHello("John");
		String msg2 = help.sayHello("John");
		assertFalse("Should NOT be the same instance.", msg == msg2);
	}
	
	public static class ShouldFillMemoizeCacheHelper{
		@Memoize(maximumSize=2)
		public String sayHello(String name){
			return "Hello " + name;
		}
	}
	
	@Test
	public void shouldFillMemoizeCache(){
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldFillMemoizeCacheHelper help = injector.getInstance(ShouldFillMemoizeCacheHelper.class);
		
		String msg = help.sayHello("One");
		help.sayHello("Two");
		help.sayHello("Three");
		String msgOrig = help.sayHello("One"); // should have been evicted
		assertFalse("Should NOT be the same instance.", msgOrig == msg);
	}
	
	public static class ShouldExpireAfter2SecondsHelper{
		@Memoize(expireAfterWriteSeconds=2)
		public String sayHello(String name){
			return "Hello " + name;
		}
	}
	
	@Test
	public void shouldExpireAfter2Seconds() throws InterruptedException{
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldExpireAfter2SecondsHelper help = injector.getInstance(ShouldExpireAfter2SecondsHelper.class);
		
		String msg = help.sayHello("One");
		Thread.sleep(2500);
		String msg2 = help.sayHello("One");
		assertFalse("Should NOT be the same instance.", msg2 == msg);
	}
	
	public static class ShouldBeSeparateCachesHelper{
		@Memoize
		public String sayHello(String name){
			return "Hello " + name;
		}
		
		@Memoize
		String sayHello(String greeting, String name){
			return greeting + " " + name;
		}
	}
	
	@Test
	public void shouldBeSeparateCaches(){
		Injector injector = Guice.createInjector(new MemoizeModule());
		ShouldBeSeparateCachesHelper help = injector.getInstance(ShouldBeSeparateCachesHelper.class);
		
		String msg = help.sayHello("One");
		String msg2 = help.sayHello("Hello", "One");
		String msg3 = help.sayHello("One");
		assertTrue("Should be the same instance", msg == msg3);
		assertFalse("Should NOT be the same instance.", msg2 == msg3);
	}
}
